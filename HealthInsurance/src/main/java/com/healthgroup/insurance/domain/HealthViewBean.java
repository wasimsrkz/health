package com.healthgroup.insurance.domain;

public class HealthViewBean {

	private String healthType;
	private String healthValue; // for yes no I would have choosen boolean yes or no
	/**
	 * @return the healthType
	 */
	public String getHealthType() {
		return healthType;
	}
	/**
	 * @param healthType the healthType to set
	 */
	public void setHealthType(String healthType) {
		this.healthType = healthType;
	}
	/**
	 * @return the healthValue
	 */
	public String getHealthValue() {
		return healthValue;
	}
	/**
	 * @param healthValue the healthValue to set
	 */
	public void setHealthValue(String healthValue) {
		this.healthValue = healthValue;
	}
	
}
