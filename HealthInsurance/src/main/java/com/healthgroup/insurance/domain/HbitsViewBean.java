package com.healthgroup.insurance.domain;

public class HbitsViewBean {

	private String habitType;
	private String habitValue;
	/**
	 * @return the habitType
	 */
	public String getHabitType() {
		return habitType;
	}
	/**
	 * @param habitType the habitType to set
	 */
	public void setHabitType(String habitType) {
		this.habitType = habitType;
	}
	/**
	 * @return the habitValue
	 */
	public String getHabitValue() {
		return habitValue;
	}
	/**
	 * @param habitValue the habitValue to set
	 */
	public void setHabitValue(String habitValue) {
		this.habitValue = habitValue;
	}
	
}
