package com.healthgroup.insurance.domain;

import java.util.List;

public class BeneficiaryViewBean {

	private String name;
	private String gender;
	private Integer age;
	private List<HealthViewBean> healthViewBeans;
	private List<HbitsViewBean> hbitsViewBeans;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}
	/**
	 * @return the healthViewBeans
	 */
	public List<HealthViewBean> getHealthViewBeans() {
		return healthViewBeans;
	}
	/**
	 * @param healthViewBeans the healthViewBeans to set
	 */
	public void setHealthViewBeans(List<HealthViewBean> healthViewBeans) {
		this.healthViewBeans = healthViewBeans;
	}
	/**
	 * @return the hbitsViewBeans
	 */
	public List<HbitsViewBean> getHbitsViewBeans() {
		return hbitsViewBeans;
	}
	/**
	 * @param hbitsViewBeans the hbitsViewBeans to set
	 */
	public void setHbitsViewBeans(List<HbitsViewBean> hbitsViewBeans) {
		this.hbitsViewBeans = hbitsViewBeans;
	}
	
}
