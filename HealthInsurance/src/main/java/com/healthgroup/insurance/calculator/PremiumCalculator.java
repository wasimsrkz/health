package com.healthgroup.insurance.calculator;

import com.healthgroup.insurance.domain.BeneficiaryViewBean;

public interface PremiumCalculator {

	Double calculatePremium(BeneficiaryViewBean beneficiaryViewBean);

}
