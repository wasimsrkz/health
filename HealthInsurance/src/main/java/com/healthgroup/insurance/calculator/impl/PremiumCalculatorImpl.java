package com.healthgroup.insurance.calculator.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.healthgroup.insurance.calculator.PremiumCalculator;
import com.healthgroup.insurance.constants.HIConstants;
import com.healthgroup.insurance.domain.BeneficiaryViewBean;
import com.healthgroup.insurance.domain.HbitsViewBean;
import com.healthgroup.insurance.domain.HealthViewBean;

@Service
public class PremiumCalculatorImpl implements PremiumCalculator {

	@Override
	public Double calculatePremium(BeneficiaryViewBean beneficiaryViewBean) {

		Integer percentage = this.getPercentagetobeAdded(beneficiaryViewBean.getAge(), beneficiaryViewBean.getGender(),
				beneficiaryViewBean.getHealthViewBeans(), beneficiaryViewBean.getHbitsViewBeans());
		
		
		return (double) (percentage/100*HIConstants.BASIC_PREMIUM);
	}

	private Integer getPercentagetobeAdded(Integer age, String gender, List<HealthViewBean> healthViewBeans,
			List<HbitsViewBean> hbitsViewBeans) {

		Map<String, String> habits = hbitsViewBeans.stream()
				.collect(Collectors.toMap(HbitsViewBean::getHabitValue, HbitsViewBean::getHabitType));

		Map<String, String> healths = healthViewBeans.stream()
				.collect(Collectors.toMap(HealthViewBean::getHealthValue, HealthViewBean::getHealthType));

		Integer percentage = 0;

		if (age > 18 && age < 40) {

			percentage += 10;
		}

		if (age > 40) {

			percentage += 20;
			int factor = (age - 40) / 5;
			percentage += (20 * factor);
		}

		if (habits.containsKey("Yes")) {

			percentage += 3;
		}

		if (healths.containsKey("Yes")) {

			percentage -= 3;
		}

		if(gender.equals("Male")) {
			
			percentage += 1;
		}
		
		return percentage;
	}
}
