package com.healthgroup.insurance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.healthgroup.insurance.calculator.PremiumCalculator;
import com.healthgroup.insurance.domain.BeneficiaryViewBean;

@RestController
@RequestMapping("/insurancecontroller")
public class InsuranceController {

	/**
	 * inject PremiumCalculator
	 */
	@Autowired
	private PremiumCalculator premiumCalculator;
	
	
	/**
	 * 
	 * @param beneficiaryViewBean
	 * @return
	 */
	
	@GetMapping("/get/premium")
	public @ResponseBody String getPremium(@RequestBody BeneficiaryViewBean beneficiaryViewBean) {
		
		Double premium = premiumCalculator.calculatePremium(beneficiaryViewBean);
		return null;
	}
	
}
